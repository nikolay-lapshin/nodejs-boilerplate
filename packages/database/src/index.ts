import { Pool, PoolClient } from 'pg';

import { DBEntityUser } from './entities';
import { IDB } from './model';

export class DB implements IDB {
  private pool: Pool;
  private client: PoolClient;

  public entities: {
    user: DBEntityUser
  };

  async start(): Promise<boolean> {
    this.pool = new Pool({
      max: 20,
      connectionString: 'postgres://ivuser:ivpass@localhost:5432/ivdb',
      idleTimeoutMillis: 30000
    });

    this.client = await this.pool.connect();

    this.entities = {
      user: new DBEntityUser(this.client)
    };

    console.log('database started');

    return Promise.resolve(true);
  }
}