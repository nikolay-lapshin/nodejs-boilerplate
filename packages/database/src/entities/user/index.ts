import { PoolClient } from 'pg';

export class DBEntityUser {
  constructor(private client: PoolClient) {}

  async list() {
    const sql = 'SELECT * FROM users';
    const { rows } = await this.client.query(sql);

    return rows;
  }
}