
import { DBEntityUser } from './entities';

export interface IDB {
  entities: {
    user: DBEntityUser
  };

  start(): Promise<boolean>;
}