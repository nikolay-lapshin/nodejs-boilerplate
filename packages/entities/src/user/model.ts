export interface IUser {
  id: string;
  name: string;
  pubname: string;
}

export interface IUserMe {
  name: string;
  pubname: string;
}