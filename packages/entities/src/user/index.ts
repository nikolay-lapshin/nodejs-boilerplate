import { IUser, IUserMe } from './model'

export class User implements IUser {
  constructor(private _id: string, private _name: string) {

  }

  get id(): string {
    return this._id;
  }

  get name(): string {
    return this._name;
  }

  get pubname(): string {
    return this._name.toLowerCase();
  }

  me(): IUserMe {
    return {
      name: this.name,
      pubname: this.pubname
    }
  }
}