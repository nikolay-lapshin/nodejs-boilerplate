import { IDB } from '@iv/database/model';

declare module 'fastify' {
  interface FastifyInstance {
    db: IDB
  }
}