import { DB } from '@iv/database';

import { apiServerConfig } from './config';
import { APIServer } from './server';

(async () => {
  const db = new DB();
  const server = new APIServer(apiServerConfig, db);

  await server.start();
  await db.start();
})();