import fastify from 'fastify';
import { FastifyInstance } from 'fastify';

import { IDB } from '@iv/database/model';

import { IAPIServerConfig } from '../config/model';
import { routes } from '../routes';

export class APIServer {
  private server: FastifyInstance;

  constructor(private config: IAPIServerConfig, db: IDB) {
    this.server = fastify({
      logger: this.config.logger.enabled
    });

    this.server.decorate('db', db);
  }

  get app(): FastifyInstance {
    return this.server;
  }

  async start() {
    this.server.register(routes);

    await this.server.listen(this.config.port);

    console.log(`started API server by port ${this.config.port}`);
  }
}