import { IAPIServerConfig } from './model';

export const apiServerConfig: IAPIServerConfig = {
  port: 9001,
  logger: {
    enabled: process.env.NODE_ENV !== 'development'
  }
};