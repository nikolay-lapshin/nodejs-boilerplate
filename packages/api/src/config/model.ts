export interface IAPIServerConfig {
  port: number;
  logger: {
    enabled: boolean
  };
}