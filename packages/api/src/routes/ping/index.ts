import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';

export async function pingRoute(fastify: FastifyInstance) {
  fastify.get('/ping', (_request: FastifyRequest, reply: FastifyReply) => {
    reply.send('pong');
  });

  return Promise.resolve();
}