import { FastifyInstance } from 'fastify';

import { pingRoute } from './ping';
import { userRoute } from './user';

export async function routes(fastify: FastifyInstance) {
  fastify.register(pingRoute, { prefix: '/api/v1' });
  fastify.register(userRoute, { prefix: '/api/v1' });

  return Promise.resolve();
}