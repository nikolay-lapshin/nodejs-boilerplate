import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';

import { User } from '@iv/entities';

const user = new User('1', 'user');

export async function userRoute(fastify: FastifyInstance) {
  fastify.get('/me', (_request: FastifyRequest, reply: FastifyReply) => {
    reply.send(user.me());
  });

  return Promise.resolve();
}